import React from 'react';
import WeatherListContainer from './components/WeatherListContainer';
import AppBar from './components/AppBar'

const App = () => {
  return (
    <React.Fragment>
      <AppBar/>
      <WeatherListContainer/>
    </React.Fragment>
  )
};

export default App;
