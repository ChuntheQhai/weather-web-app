const api = process.env.REACT_APP_WEATHER_API_KEY || "dbb624c32c7f0d652500552c5ebbde56"

export const fetchWeatherAPI = (city) => {
    return fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=${api}&units=metric`)
}