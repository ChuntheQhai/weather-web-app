import { createStore, combineReducers } from "redux";
import shortid from "shortid";

export const CREATE_WEATHER = "CREATE_WEATHER";
export const UPDATE_WEATHER = "UPDATE_WEATHER";
export const DELETE_WEATHER = "DELETE_WEATHER";

export const createWeather = details => ({
  type: CREATE_WEATHER,
  payload: { details }
});

export const updateWeather = (id, details) => ({
  type: UPDATE_WEATHER,
  payload: { id, details }
});

export const removeWeather = id => ({
  type: DELETE_WEATHER,
  payload: { id }
});

const weatherReducer = (state = {}, action) => {
  switch (action.type) {
    case CREATE_WEATHER: {
      const { details } = action.payload;

      return {
        ...state,
        [shortid()]: details
      };
    }

    case UPDATE_WEATHER: {
      const { id, details } = action.payload;
      return {
        ...state,
        [id]: details
      };
    }

    case DELETE_WEATHER: {
      const { id } = action.payload;
      console.log({
        ...state,
        [id]: undefined
      })
      return {
        ...state,
        [id]: undefined
      };
    }

    default:
      return state;
  }
};

export const store = createStore(
  combineReducers({
    weather: weatherReducer
  }),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);


// const now = Math.round((new Date()).getTime() / 1000);
// console.log('now? ', now)
// const rawData = [
//   {"_id": 100, "city":"Shah Alam","interval":10000,"updatedAt": 1593842400,"result":{"cod":"200","message":0,"cnt":40,"list":[{"dt":1593788400,"main":{"temp":28.02,"feels_like":33.13,"temp_min":25.94,"temp_max":28.02,"pressure":1009,"sea_level":1010,"grnd_level":1006,"humidity":79,"temp_kf":2.08},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":59},"wind":{"speed":1.03,"deg":107},"rain":{"3h":0.12},"sys":{"pod":"n"},"dt_txt":"2020-07-03 15:00:00"},{"dt":1593799200,"main":{"temp":26.21,"feels_like":31.24,"temp_min":25.17,"temp_max":26.21,"pressure":1009,"sea_level":1009,"grnd_level":1005,"humidity":84,"temp_kf":1.04},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],"clouds":{"all":83},"wind":{"speed":0.53,"deg":139},"sys":{"pod":"n"},"dt_txt":"2020-07-03 18:00:00"},{"dt":1593885600,"main":{"temp":25.05,"feels_like":29.76,"temp_min":25.05,"temp_max":25.05,"pressure":1010,"sea_level":1010,"grnd_level":1006,"humidity":88,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":99},"wind":{"speed":0.7,"deg":350},"sys":{"pod":"n"},"dt_txt":"2020-07-04 18:00:00"},{"dt":1593972000,"main":{"temp":24.13,"feels_like":28.35,"temp_min":24.13,"temp_max":24.13,"pressure":1008,"sea_level":1008,"grnd_level":1004,"humidity":89,"temp_kf":0},"weather":[{"id":802,"main":"Clouds","description":"scattered clouds","icon":"03n"}],"clouds":{"all":48},"wind":{"speed":0.84,"deg":102},"sys":{"pod":"n"},"dt_txt":"2020-07-05 18:00:00"},{"dt":1594058400,"main":{"temp":25.09,"feels_like":29.51,"temp_min":25.09,"temp_max":25.09,"pressure":1009,"sea_level":1009,"grnd_level":1005,"humidity":84,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":99},"wind":{"speed":0.54,"deg":63},"rain":{"3h":0.71},"sys":{"pod":"n"},"dt_txt":"2020-07-06 18:00:00"},{"dt":1594144800,"main":{"temp":24.41,"feels_like":27.44,"temp_min":24.41,"temp_max":24.41,"pressure":1010,"sea_level":1010,"grnd_level":1006,"humidity":84,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":100},"wind":{"speed":2.03,"deg":127},"sys":{"pod":"n"},"dt_txt":"2020-07-07 18:00:00"}],"city":{"id":1732903,"name":"Shah Alam","coord":{"lat":3.0851,"lon":101.5328},"country":"MY","population":481654,"timezone":28800,"sunrise":1593731346,"sunset":1593775607}}},
//   // {"_id": 200, "city":"Shah Alam","interval":5000,"updatedAt": now,"result":{"cod":"200","message":0,"cnt":40,"list":[{"dt":1593788400,"main":{"temp":28.02,"feels_like":33.13,"temp_min":25.94,"temp_max":28.02,"pressure":1009,"sea_level":1010,"grnd_level":1006,"humidity":79,"temp_kf":2.08},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":59},"wind":{"speed":1.03,"deg":107},"rain":{"3h":0.12},"sys":{"pod":"n"},"dt_txt":"2020-07-03 15:00:00"},{"dt":1593799200,"main":{"temp":26.21,"feels_like":31.24,"temp_min":25.17,"temp_max":26.21,"pressure":1009,"sea_level":1009,"grnd_level":1005,"humidity":84,"temp_kf":1.04},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],"clouds":{"all":83},"wind":{"speed":0.53,"deg":139},"sys":{"pod":"n"},"dt_txt":"2020-07-03 18:00:00"},{"dt":1593885600,"main":{"temp":25.05,"feels_like":29.76,"temp_min":25.05,"temp_max":25.05,"pressure":1010,"sea_level":1010,"grnd_level":1006,"humidity":88,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":99},"wind":{"speed":0.7,"deg":350},"sys":{"pod":"n"},"dt_txt":"2020-07-04 18:00:00"},{"dt":1593972000,"main":{"temp":24.13,"feels_like":28.35,"temp_min":24.13,"temp_max":24.13,"pressure":1008,"sea_level":1008,"grnd_level":1004,"humidity":89,"temp_kf":0},"weather":[{"id":802,"main":"Clouds","description":"scattered clouds","icon":"03n"}],"clouds":{"all":48},"wind":{"speed":0.84,"deg":102},"sys":{"pod":"n"},"dt_txt":"2020-07-05 18:00:00"},{"dt":1594058400,"main":{"temp":25.09,"feels_like":29.51,"temp_min":25.09,"temp_max":25.09,"pressure":1009,"sea_level":1009,"grnd_level":1005,"humidity":84,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":99},"wind":{"speed":0.54,"deg":63},"rain":{"3h":0.71},"sys":{"pod":"n"},"dt_txt":"2020-07-06 18:00:00"},{"dt":1594144800,"main":{"temp":24.41,"feels_like":27.44,"temp_min":24.41,"temp_max":24.41,"pressure":1010,"sea_level":1010,"grnd_level":1006,"humidity":84,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":100},"wind":{"speed":2.03,"deg":127},"sys":{"pod":"n"},"dt_txt":"2020-07-07 18:00:00"}],"city":{"id":1732903,"name":"Shah Alam","coord":{"lat":3.0851,"lon":101.5328},"country":"MY","population":481654,"timezone":28800,"sunrise":1593731346,"sunset":1593775607}}},
//   // {"_id": 300, "city":"Shah Alam","interval":15000,"updatedAt": now,"result":{"cod":"200","message":0,"cnt":40,"list":[{"dt":1593788400,"main":{"temp":28.02,"feels_like":33.13,"temp_min":25.94,"temp_max":28.02,"pressure":1009,"sea_level":1010,"grnd_level":1006,"humidity":79,"temp_kf":2.08},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":59},"wind":{"speed":1.03,"deg":107},"rain":{"3h":0.12},"sys":{"pod":"n"},"dt_txt":"2020-07-03 15:00:00"},{"dt":1593799200,"main":{"temp":26.21,"feels_like":31.24,"temp_min":25.17,"temp_max":26.21,"pressure":1009,"sea_level":1009,"grnd_level":1005,"humidity":84,"temp_kf":1.04},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],"clouds":{"all":83},"wind":{"speed":0.53,"deg":139},"sys":{"pod":"n"},"dt_txt":"2020-07-03 18:00:00"},{"dt":1593885600,"main":{"temp":25.05,"feels_like":29.76,"temp_min":25.05,"temp_max":25.05,"pressure":1010,"sea_level":1010,"grnd_level":1006,"humidity":88,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":99},"wind":{"speed":0.7,"deg":350},"sys":{"pod":"n"},"dt_txt":"2020-07-04 18:00:00"},{"dt":1593972000,"main":{"temp":24.13,"feels_like":28.35,"temp_min":24.13,"temp_max":24.13,"pressure":1008,"sea_level":1008,"grnd_level":1004,"humidity":89,"temp_kf":0},"weather":[{"id":802,"main":"Clouds","description":"scattered clouds","icon":"03n"}],"clouds":{"all":48},"wind":{"speed":0.84,"deg":102},"sys":{"pod":"n"},"dt_txt":"2020-07-05 18:00:00"},{"dt":1594058400,"main":{"temp":25.09,"feels_like":29.51,"temp_min":25.09,"temp_max":25.09,"pressure":1009,"sea_level":1009,"grnd_level":1005,"humidity":84,"temp_kf":0},"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10n"}],"clouds":{"all":99},"wind":{"speed":0.54,"deg":63},"rain":{"3h":0.71},"sys":{"pod":"n"},"dt_txt":"2020-07-06 18:00:00"},{"dt":1594144800,"main":{"temp":24.41,"feels_like":27.44,"temp_min":24.41,"temp_max":24.41,"pressure":1010,"sea_level":1010,"grnd_level":1006,"humidity":84,"temp_kf":0},"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04n"}],"clouds":{"all":100},"wind":{"speed":2.03,"deg":127},"sys":{"pod":"n"},"dt_txt":"2020-07-07 18:00:00"}],"city":{"id":1732903,"name":"Shah Alam","coord":{"lat":3.0851,"lon":101.5328},"country":"MY","population":481654,"timezone":28800,"sunrise":1593731346,"sunset":1593775607}}}
// ]
// // // const chance = new Chance();
// for (let i = 0; i < rawData.length; ++i) {
//   store.dispatch(
//     createWeather(rawData[i])
//   );
// }
