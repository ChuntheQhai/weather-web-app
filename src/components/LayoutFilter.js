import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  }
}));

export default function ControlledOpenSelect({ layoutType, open, handleChange, handleClose, handleOpen }) {
  const classes = useStyles();
  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-controlled-open-select-label">Layout Type</InputLabel>
        <Select
          labelId="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={layoutType}
          onChange={handleChange}
        >
          <MenuItem value={"autoLayout"}>
            <em>Auto-Layout</em>
          </MenuItem>
          <MenuItem value={"OneColumPerRow"}>1 column per row</MenuItem>
          <MenuItem value={"ThreeColumPerRow"}>2 column per row</MenuItem>
          <MenuItem value={"ThreeColumPerRow"}>3 column per row</MenuItem>
          <MenuItem value={"FourColumPerRow"}>4 column per row</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}