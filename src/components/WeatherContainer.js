import React from "react"
import { connect } from "react-redux"
import { useModal } from "react-modal-hook"
import Grid from '@material-ui/core/Grid'
import { removeWeather, updateWeather } from "./../redux"
import Dialog from "./Dialog"
import WeatherFormContainer from "./WeatherFormContainer"
import WeatherCard from "./WeatherCard"
import useInterval from './../hooks/useInterval'
import { getDayByDay } from './../utils/helper'
import { fetchWeatherAPI } from './../api/weather'

const enhance = connect(
  (state, ownProps) => ({
    weather: state.weather[ownProps.id]
  }),
  { removeWeather, updateWeather }
);

const WeatherContainer = ({ id, weather, removeWeather, updateWeather }) => {
  const [stopIntervalRunning] = useInterval(() => {
    if(weather.city){
      return fetchWeatherAPI(weather.city)
        .then(res => res.json())
        .then(
          (result) => {
            if (Object.keys(result).length > 0) {
              result["list"] = getDayByDay(result["list"])
              updateWeather(id, {
                ...weather,
                "updatedAt": new Date().toString(),
                result
              })
            }
          }
        )
    }
  }, weather["interval"])

  const [, hideEditModal] = useModal(({ in: open, onExited }) => (
    <WeatherFormContainer
      id={id}
      open={open}
      onExited={onExited}
      onClose={hideEditModal}
    />
  ));

  const [showConfirmModal, hideConfirmModal] = useModal(
    ({ in: open, onExited }) => (
        <Dialog
          open={open}
          onExited={onExited}
          title="Delete weather?"
          confirmLabel="Delete"
          onConfirm={() => {
            stopIntervalRunning();
            setTimeout(() => removeWeather(id), 1000)
          }}
          onCancel={hideConfirmModal}
      >
        Do you really want to delete {weather.city}?
      </Dialog>
    ),
    [weather]
  );
    
  return (
    <Grid item xs>
      <WeatherCard
        forecast={weather.result.list}
        city={weather.city}
        showConfirmModal={showConfirmModal}
        updatedAt={weather["updatedAt"]}
      />
    </Grid>
  );
};

export default enhance(WeatherContainer);
