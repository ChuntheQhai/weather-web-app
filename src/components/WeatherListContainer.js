import React, { useState } from "react";
import { connect } from "react-redux";
import Grid from '@material-ui/core/Grid';
import WeatherContainer from "./WeatherContainer";
import { makeStyles } from '@material-ui/core/styles';
import LayoutFilter from './LayoutFilter'

const useStyles = makeStyles({
  root: {
    marginTop: 76,
  },
  autoLayout: {
    display: "flex"
  },
  OneColumPerRow: {
    display: "grid",
    gridTemplateColumns: "repeat(1, minmax(100px, 1fr))"
  },
  TwoColumPerRow: {
    display: "grid",
    gridTemplateColumns: "repeat(2, minmax(100px, 1fr))"
  },
  ThreeColumPerRow: {
    display: "grid",
    gridTemplateColumns: "repeat(3, minmax(100px, 1fr))"
  },
  FourColumPerRow: {
    display: "grid",
    gridTemplateColumns: "repeat(4, minmax(100px, 1fr))"
  }
});

const enhance = connect(state => ({
  ids: Object.keys(state.weather)
    .filter(id => state.weather[id])
    .reverse()
}));

const WeatherListContainer = ({ ids }) => {
  const classes = useStyles();
  const [layoutType, setLayoutType] = useState('autoLayout')
  const [open, setOpen] = useState(false);

  const handleChange = (event) => setLayoutType(event.target.value);
  const handleClose = () => setOpen(false);
  const handleOpen = () => setOpen(true);
  
  return (
    <div className={classes.root}>
      <LayoutFilter
        layoutType={layoutType}
        open={open}
        handleChange={handleChange}
        handleClose={handleClose}
        handleOpen={handleOpen}s
      />
      <Grid container spacing={1} className={classes[layoutType]}>
        {ids.map(id => (
          <WeatherContainer key={id} id={id} />
        ))}
      </Grid>
    </div>
  );
};

export default enhance(WeatherListContainer);
