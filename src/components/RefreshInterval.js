import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import Input from '@material-ui/core/Input';
import HistoryIcon from '@material-ui/icons/History';

const useStyles = makeStyles({
  root: {
    width: 250,
    margin: "0 auto",
    marginTop: 20,
    marginBottom: 20
  },
  input: {
    width: 42,
  },
});

const refreshIntervalSlider = ({ 
    intervalValue,
    handleSliderChange,
    handleInputChange,
    handleBlur
}) => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
        <Typography id="input-slider" gutterBottom>
            Refresh Interval
        </Typography>
            <Grid container spacing={2} alignItems="center">
            <Grid item>
                <HistoryIcon />
            </Grid>
            <Grid item xs>
                <Slider
                min={1}
                value={typeof intervalValue === 'number' ? intervalValue : 0}
                onChange={handleSliderChange}
                aria-labelledby="input-slider"
                />
            </Grid>
            <Grid item>
            <Input
            className={classes.input}
            value={intervalValue}
            margin="dense"
            onChange={handleInputChange}
            onBlur={handleBlur}
            inputProps={{
                step: 1,
                min: 1,
                max: 100,
                type: 'number',
                'aria-labelledby': 'input-slider',
            }}
            />Seconds
        </Grid>
        </Grid>
    </div>
    );
}


export default refreshIntervalSlider;