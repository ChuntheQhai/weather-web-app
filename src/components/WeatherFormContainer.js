import React, { useState, useCallback } from "react";
import { connect } from "react-redux";
import { snackbarService } from 'uno-material-ui';
import { makeStyles } from '@material-ui/core/styles';
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import { createWeather, updateWeather } from "./../redux";
import RefreshInterval from './RefreshInterval'
import { getDayByDay } from './../utils/helper'
import { fetchWeatherAPI } from './../api/weather'

const useStyles = makeStyles((theme) => ({
    backdrop: {
      zIndex: theme.zIndex.drawer + 999999999,
      color: '#fff',
    },
    formContent: {
        textAlign: "center"
    },
    fieldCity: {
        width: 250
    }
}));

const enhance = connect(
  (state, ownProps) => ({
    weather: ownProps.id ? state.weather[ownProps.id] : undefined,
    weatherCities: Object.values(state.weather).filter(w => w !== undefined).map(w => w.city.toLowerCase())
  }),
  { createWeather, updateWeather }
);

const WeatherFormContainer = ({
  id,
  weather,
  createWeather,
  updateWeather,
  onClose,
  weatherCities,
  ...rest
}) => {
    const classes = useStyles();
    const [cityValidate, setCityValidate] = useState(false)
    const [creating, setCreating] = useState(false);
    const [intervalValue, setInterval] = useState(30)

    const handleSliderChange = (event, newValue) => {
        return newValue < 1 ? setInterval(1) : setInterval(newValue)
    };
    
    const handleInputChange = (event) => {
        return (event && Number(event.target.value) < 1) ? setInterval(1) : setInterval(event.target.value)
    };

    const handleBlur = () => {
        if (intervalValue < 0) {
            setInterval(0);
        } else if (intervalValue > 100) {
            setInterval(100);
        }
    };

    const getWeather = async (city) => {
        return new Promise(async (resolve, reject) => {
            if (!city || (city && weatherCities.includes(city.trim().toLowerCase()))) {
                setCityValidate(true)
                return;
            }
            
            try {
                setCreating(true)
                const result = await fetchWeatherAPI(city)
                setCreating(false)
                resolve(
                    result["status"] === 200 ? result.json() : snackbarService.showSnackbar(`Ops! ${result.statusText}!`, 'error')
                )
            } 
            catch(err){
                snackbarService.showSnackbar(`Ops! ${err.message}!`, 'error');
                reject(err)
            }     
        })
    }

    const handleSubmit = useCallback(async e => {
        e.preventDefault();
        let data = {
            city: e.target.city.value,
            interval: intervalValue * 1000,
        }
        let weatherData = await getWeather(data["city"]);
        weatherData["list"] = getDayByDay(weatherData["list"])
        data["result"] = weatherData
        data["updatedAt"] = new Date().toString()
        createWeather(data);
        onClose();
    }, [intervalValue]);
    
    return (
        <Dialog
            {...rest}
            onClose={onClose}
            aria-labelledby="person-dialog-slide-title"
            fullWidth={true}
        >
            <Backdrop className={classes.backdrop} open={creating}>
                <CircularProgress color="inherit" />Creating
            </Backdrop>
            <form onSubmit={handleSubmit}>                
                <DialogTitle id="person-dialog-slide-title">
                    {id ? "Edit Weather" : "Create Weather"}
                </DialogTitle>
                <DialogContent className={classes.formContent}>
                    <TextField
                        className={classes.fieldCity}
                        error={ cityValidate === true }
                        helperText={ cityValidate ? "Your city is empty or duplicate." : ''}
                        name="city"
                        label="City"
                        placeholder="Enter Your City"
                        defaultValue={weather ? weather.city : ""}
                    />
                    <RefreshInterval
                        setInterval={setInterval}
                        intervalValue={intervalValue}
                        handleSliderChange={handleSliderChange}
                        handleInputChange={handleInputChange}
                        handleBlur={handleBlur}
                    />
                </DialogContent>
                <DialogActions>
                <Button onClick={onClose}>Cancel</Button>
                <Button type="submit" color="primary">
                Add
                </Button>
            </DialogActions>
            </form>
        </Dialog>
    );
};

export default enhance(WeatherFormContainer);
