import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import InvertColorsIcon from '@material-ui/icons/InvertColors';
import ExploreIcon from '@material-ui/icons/Explore';
import DeleteIcon from '@material-ui/icons/Delete';
import useOnHover from './../hooks/useOnHover'
import moment from 'moment'

import {
  FiCard,
  FiCardActionArea,
  FiCardActions,
  FiCardContent,
  FiCardMedia
} from "./../styles/FullImageCard";

const CELSIUS = "C"
const useStyles = makeStyles({
  title: {
    fontSize: 14,
    color: "white"
  },
  updated: {
    float: "right",
    color: "white"
  },
  paper: {
    borderRadius: 0,
    textAlign: "center",
    paddingTop: 15,
    paddingBottom: 15,
    fontSize: 44
  },
  fiCardContent: {
    color: "#ffffff",
    backgroundColor: "rgba(0,0,0,.24)"
  },
  cardAction: {
    padding: 0
  },
  imageButtonWrapper: {
    height: "100%",
    width: "100%",
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    backdropFilter: "blur(2px)",
    zIndex: 1
  },
  removeBtn: {
    zIndex: 2,
    position: "relative"
  },
  updatedSince: {
    display: "block",
    fontSize: 12
  },
  updatedTime: {
    fontSize: 18
  },
  weatherSituation: {
    marginRight: 15
  },
  weatherSituationIcon: {
    verticalAlign: 'middle'
  },
  weatherSituationValue: {
    verticalAlign: 'text-top',
    marginLeft: 5
  },
  weatherHumidity: {
    verticalAlign: "top",
    marginRight: 5
  },
  weatherIcon: {
    marginLeft: 15
  },
  forecaseIcon: {
    fontSize: 32
  }
});

const WeatherCard = ({ forecast, city, showConfirmModal, updatedAt }) => {
  const classes = useStyles();
  const today = (forecast) ? forecast[0] : {}
  const [hover, hoverRef] = useOnHover();

  const getDay = (index) => {
    const days = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'];
    return days[index];
  }

  const getDateString = (seconds) => {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const date = new Date(seconds * 1000);
    return `${getDay(date.getDay())}, ${months[date.getMonth()]} ${date.getDate()} ${date.getFullYear()}`;
  }

  const getTimeString = (date) => {
    return moment(date).format('hh:mm:ss A')
  }

  const getCurrentTemp = (temps) => {
    return Math.round(temps);
  }

  const isDay = () => {
    const hours = new Date(Date.now()).getHours();
    return hours >= 7 && hours <= 20 ? true : false;
  }
  
  const renderImage = (today) => {
    let wId = today["id"]
    if (wId >= 200 && wId <= 299) return "clear.jpg"
    else if (wId >= 200 && wId <= 299) return "thunderstorm.jpg"
    else if (wId >= 300 && wId <= 399) return "drizzle.jpg"
    else if (wId >= 500 && wId <= 599) return "rain.jpg"
    else if (wId >= 600 && wId <= 699) return "snow.jpg"
    else if (wId >= 700 && wId <= 799) return "mist.jpg"
    else if (wId === 800) return "clear.jpg"
    else if (wId > 800 && wId <= 899) return "clouds.jpg"
    else return "clear.jpg"
  }

  return (
    <FiCard raised={hover} ref={hoverRef}>
      { hover && (
        <div className={classes.imageButtonWrapper}>
          <Button startIcon={<DeleteIcon />} onClick={showConfirmModal} className={classes.removeBtn} variant="contained" color="secondary">
            Remove
          </Button>
        </div>
      )}
      <FiCardActionArea>
        <FiCardMedia
          media="picture"
          image={`/images/${renderImage(today.weather[0])}`}
        />
        <FiCardContent className={classes.fiCardContent}>
          <Typography align="left" display="inline" className={classes.title} color="textSecondary" gutterBottom>
            {city.toUpperCase()}
          </Typography>
          <Typography align="right"  display="inline" className={classes.updated} color="textSecondary" gutterBottom>
            <span className={classes.updatedSince}>Updated since</span>
            <span className={classes.updatedTime}>{getTimeString(updatedAt)}</span>
          </Typography>

          <Typography variant="h4" component="h4">
            {getDateString(today.dt)}
          </Typography>
          <Typography variant="h5" component="h5">
            {today.weather[0].description}
          </Typography>
          <Typography  variant="body1" className={classes.weatherHumidity}>
            <InvertColorsIcon className={classes.weatherSituationIcon}/>
            <span className={classes.weatherSituationValue}>Humidity {`${today.main.humidity}`} %</span>
          </Typography>
          <Typography  variant="body1" className={classes.weatherSituation}>
            <ExploreIcon className={classes.weatherSituationIcon}/>
            <span className={classes.weatherSituationValue}>Wind Degree {`${today.wind.deg}`} deg</span>
          </Typography>
          <Typography  variant="body1" className={classes.weatherSituation}>
            <i className={`wi wi-owm-905 ${classes.weatherSituationIcon}`}/>
            <span className={classes.weatherSituationValue}>Wind Speed {`${today.wind.speed}`} m/s</span>
          </Typography>
          <Typography variant="h1" component="h1" style={{ marginTop: 15 }}>
            {`${getCurrentTemp(today.main.temp)}°${CELSIUS}`}
            <i className={`wi wi-owm-${isDay() ? 'day' : 'night'}-${today.weather[0].id} ${classes.weatherIcon}`}></i>
          </Typography>
        </FiCardContent>
      </FiCardActionArea>
      <FiCardActions className={classes.cardAction}>
        <Grid container spacing={0}>
          {forecast.map((day, i) => (
            <Grid item xs={2} key={i}>
              <Paper className={classes.paper} elevation={1} key={day.dt}>
                <Typography variant="body1">
                  {getDay(new Date(day.dt * 1000).getDay())}
                </Typography>
                <i className={`wi wi-owm-${day.weather[0].id} ${classes.forecaseIcon}`}></i>
                <Typography variant="body1">
                  {`${getCurrentTemp(day.main.temp)}°${CELSIUS}`}
                </Typography>
              </Paper>
            </Grid>
          ))}
        </Grid>
      </FiCardActions>
    </FiCard>
  )
}
export default WeatherCard;