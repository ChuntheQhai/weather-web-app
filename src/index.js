import React from "react";
import ReactDOM from "react-dom";
import 'weathericons/css/weather-icons.min.css';
import { createMuiTheme, responsiveFontSizes, ThemeProvider } from '@material-ui/core/styles';
import { Provider } from "react-redux";
import { ModalProvider } from "react-modal-hook";
import { TransitionGroup } from "react-transition-group";
import { SnackbarContainer } from 'uno-material-ui';
import * as serviceWorker from './serviceWorker'
import { store } from "./redux";
import App from './App';


let theme = createMuiTheme();
theme = responsiveFontSizes(theme);

ReactDOM.render(
  <Provider store={store}>
    <ModalProvider rootComponent={TransitionGroup}>
      <ThemeProvider theme={theme}>
        <SnackbarContainer/>
        <App />
      </ThemeProvider>
    </ModalProvider>
  </Provider>,
  document.getElementById("root")
);

if(process.env.NODE_ENV === 'production') {
  serviceWorker()
}
