import { useState, useEffect, useRef, useCallback } from 'react';

export default function useInterval(callback, delay, immediate = true) {
  const savedCallback = useRef();
  const intervalId = useRef(null);
  const [currentDelay, setDelay] = useState(delay);

  const stopIntervalRunning = useCallback(
    () => setDelay(currentDelay => (currentDelay === null ? delay : null)),
    [delay]
  );
  
  const clear = useCallback(() => 
    clearInterval(intervalId.current)
  , []);

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    (async() => {
      async function tick() {
        await savedCallback.current();
      }
      if (intervalId.current) clear();

      if (currentDelay !== null) {
        if (immediate) {
          await tick();
        }
        intervalId.current = setInterval(tick, currentDelay);
      }
      return clear;
    })(); 
  }, [currentDelay, clear]);
  return [stopIntervalRunning]
}