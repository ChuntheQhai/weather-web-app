export const getDayByDay = (data) => {
    const days = new Set([])
    return data.reduce((a, d, i) => {
        const day = new Date(d.dt * 1000).getDay();
        if(!days.has(day)) {
            a.push(d)
            days.add(day)
        }
        return a
    }, [])
}