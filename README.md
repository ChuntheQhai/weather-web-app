React Weather Web Application
==================================

Simple React Weather Application

Requirement
---------------
```
Node >= 7.10.1
```

Getting Started
---------------

```sh
# Install dependencies
npm install

# Start development server
npm start

# Build & start production server:
npm run build && cd build && python -m SimpleHTTPServer 3333
```

Environment Variables
---------------
Create `.env` file in project root folder and fill in your environment variable to override the default value.


| Variable                           | Default                                    | Required |
| ---------------------------------- | ------------------------------------------ | -------- |
| REACT_APP_WEATHER_API_KEY          | dbb624c32c7f0d652500552c5ebbde56           | YES      |


Source Codes Tested
---------------
```
Node: v14.4.0
NPM: v6.14.4
Chrome: v83.0.4103.116 
Operating System: Mac OS 10.15.4
```

